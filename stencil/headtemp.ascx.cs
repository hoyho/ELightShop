﻿using System;
using System.Data;
using System.Data.SqlClient;

using YduCLB.SQL;

public partial class stencil_headtemp : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
		int nQty = 0;
        
        if (Request.Cookies["Currency"] != null)
        {
            Ft_Curr.Text = "<span class=\"currency " + System.Web.HttpContext.Current.Request.Cookies["Currency"].Value + "\"></span>";
        }
        else
        {
            Ft_Curr.Text = "<span class=\"currency\"></span>";
        }

        try
        {
            SqlDataReader drReader = Tc.ConnDate("select count(id) qty from " + TableName.db_cart + " where ident='" + Request.Cookies["MyCartIds"].Value + "'");
            if (drReader.Read())
            {
                nQty = Convert.ToInt16(drReader["qty"]);

            }
            drReader.Close();
            drReader.Dispose();
        }
        catch
        {

        }
        Ft_CartQty.Text = nQty.ToString();
    }
}