﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="headtemp.ascx.cs" Inherits="stencil_headtemp" %>
<div id="HeadDiv">
	<div class="logo"><a href="/"><img alt="China Tablet PCs Sale" src="/stylecss/images/logo.jpg" /></a></div>
    <div class="entry">
      <div class="v1" id="entry_elem01">
	      <span class="i1">Currency: <span class="currbtn"><asp:Literal id="Ft_Curr" runat="server"/><i class="btn"></i><span class="selerate" style="display: none;"><i class="USD"></i><i class="GBP"></i><i class="AUD"></i><i class="CAD"></i><i class="EUR"></i><i class="ZAR"></i></span></span></span>
	      <span class="i2">Welcome, <a href="/account/">My Account</a> | <%if (HttpContext.Current.Session["Member"] != null){%><a href="/signout/">Sign Out</a> | <%}else{%><a href="/signin/">Sign In</a> | <a href="/register/">Register</a> | <%}%><a href="/help/">Help</a></span>
	      <span class="i3"><b onclick="location.href='/cart/'"><asp:Literal id="Ft_CartQty" runat="server"/> items</b></span>
      </div><!--v1-->
      <div class="v2">
      	<div class="ekeys">Popular: <a href="/search/?keys=Zenithink">Zenithink</a> <a href="/search/?keys=Ramos">Ramos</a> <a href="/search/?keys=Newsmy">Newsmy</a> <a href="/search/?keys=Teclast">Teclast</a></div>
        <div class="isearch">
            <form action="/search/" method="post">
            <input type="submit" name="button" id="button" value="Submit" class="btn" />
            <input type="text" name="keys-input" id="keys-input" class="inputs" onkeyup="isearch(this.value);" autocomplete="off" onblur="isearch();" />
            <p class="mclear"></p>
            <div class="tips-pop" id="tips-pop" style="display: none;"></div>
            <div class="tips-wait" id="tips-wait" style="display: none;"></div>
            </form>
        </div>
        <p class="mclear"></p>
      </div><!--v2-->
    </div><!--entry-->
    <p class="mclear"></p>
    <div class="nav" id="mainnav">
    	<div class="v1">
	        <ul class="ult">
		    	<li class="lit"><div class="nav-ti"><a href="/">Home</a></div></li>
		        <li class="lit"><div class="nav-ti"><a class="act" href="/new-arrivals/">New Arrivals</a></div></li>
		        <li class="lit">
		            <div class="nav-ti"><a class="nor" href="/android-tablets-2x/">Android 2.x</a></div>
		            <div class="nav-sub" style="display: none;">
		                <ul>
		                    <li><a href="/android-tablets-2-1/">android 2.1 tablet</a></li>
		                    <li><a href="/android-tablets-2-2/">android 2.2 tablet</a></li>
		                    <li><a href="/android-tablets-2-3/">android 2.3 tablet</a></li>
		                </ul>
		            </div>
		        </li>
		        <%--<li class="lit">
		            <div class="nav-ti"><a class="nor" href="/android-tablets-3x/">Android 3.x</a></div>
		            <div class="nav-sub" style="display: none;">
		                <ul>
		                    <li><a href="#">android 3.0 tablet</a></li>
		                    <li><a href="#">android 3.1 tablet</a></li>
		                    <li><a href="#">android 3.2 tablet</a></li>
		                </ul>
		            </div>
		        </li>--%>
		        <li class="lit">
		            <div class="nav-ti"><a class="nor" href="/android-tablets-4x/">Android 4.x</a></div>
		            <div class="nav-sub" style="display: none;">
		                <ul>
		                    <li><a href="/android-tablets-4-0/">android 4.0 tablet</a></li>
		                </ul>
		            </div>
		        </li>
		        <li class="lit"><div class="nav-ti"><a class="nor" href="/promotion/">Promotion</a></div></li>
		        <li class="lit"><div class="nav-ti"><a class="nor" href="/news/">News</a></div></li>
                <li class="mclear"></li>
	        </ul>
        </div>
        <div class="v2"></div>
        <p class="mclear"></p>
    </div><!--nav-->
</div><!--HeadDiv-->