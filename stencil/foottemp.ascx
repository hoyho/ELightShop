﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="foottemp.ascx.cs" Inherits="stencil_foottemp" %>
<div id="FootDiv">
    <div class="v1">
        <div class="tips">
            <h4>Contact US</h4>
            <p>If you have any question or need help, you may contact us to assist you. <br />
            <!--Tel:(US)<br />-->
            <span>MSN: dabpop139@hostmail.com</span><br />
            <span>Email: dabpop139@gmail.com</span></p>
        </div>
        <div class="info">
            <h4>Shoping</h4>
            <ul>
                <li><a rel="nofollow" href="/register/?return=self">Create an Account</a></li>
                <li><a rel="nofollow" href="/help/how-to-order-14.html">How to Order</a></li>
                <li><a rel="nofollow" href="/help/payment-options-15.html">Payment Options</a></li>
                <li><a rel="nofollow" href="/help/shipping-methods-11.html">Shipping Methods</a></li>
                <li><a rel="nofollow" href="/help/order-tracking-13.html">Order Tracking</a></li>
            </ul>
        </div>
        <div class="info">
            <h4>Customer Service</h4>
            <ul>
                <li><a rel="nofollow" href="/help/contact-us-2.html">Contact Us</a></li>
                <li><a rel="nofollow" href="/help/">Frequently Asked Questions</a></li>
                <li><a rel="nofollow" href="/help/products-and-services-3.html">Products & Services</a></li>
            </ul>
        </div>
        <div class="info">
            <h4>More Info</h4>
            <ul>
                <li><a rel="nofollow" href="/help/company-information-4.html">Company Information</a></li>
                <li><a rel="nofollow" href="/help/returns-policy-7.html">Returns Policy</a></li>
                <li><a rel="nofollow" href="/help/return-process-6.html">Return Process</a></li>
                <li><a rel="nofollow" href="/help/privacy-policy-5.html">Privacy Policy</a></li>
            </ul>
        </div>
        <p class="mclear"></p>
    </div><!--v1-->
    <div class="v2">
    	<span class="tw"></span><a href="#">Follow us on Twitter</a>
		<span class="fb"></span><a href="#">Like us on Facebook</a>
    </div>
    <div class="v3">
        <div class="i1"><a rel="nofollow" target="_blank" href="/?cloud=android tablets">Android Tablets</a>|<a rel="nofollow" target="_blank" href="/help/terms-of-use-1.html">Terms of Use</a>|<a rel="nofollow" target="_blank" href="/help/privacy-policy-5.html">Privacy Policy</a>|<a rel="nofollow" target="_blank" href="/help/company-information-4.html">Company Info</a>|<a rel="nofollow" target="_blank" href="/help/contact-us-2.html">Contact Us</a>|<a rel="nofollow" target="_blank" href="#">Sitemap</a>|<a target="_blank" href="/help/">Help Center</a></div>
        <div class="i2"><img alt="deliver" src="/stylecss/images/icon_deliver.gif" /><img alt="satisfaction 100% guaranteed" src="/stylecss/images/100.gif" /><img alt="secure site" src="/stylecss/images/secure-site.gif" /><img alt="verisign" src="/stylecss/images/verisign.gif" /><img src="/stylecss/images/icon_card.gif" alt="card" /></div>
        <div class="i3">Copyright &copy; 2011 OfferTablets.com. All Rights Reserved.</div>
	</div>
	<%if(isIndex){%>
    <div class="site_tips">
		<div class="ti"><h2>Why buy products from us?</h2></div>
        <div class="list">
		  <div class="dl">
		    <div class="i1">About OfferTablets.com</div>
		    <div class="i2">OfferTablets.com is a large B2C trading platform which is dedicated in providing quality android tablets to consumers at wholesale price. Android 2.1, Android 2.2, Android 2.3 tablets and different sizes of <strong>Tablet PCs</strong> are supplied at OfferTablets.com. We have the convenient purchase process and thoughtful service for you to make secure and happy purchases. </div>
		  </div>
		  <div class="dl">
		    <div class="i1">Safe and Easy</div>
		    <div class="i2">We always take the security at the first place at OfferTablets.com. When you purchase <strong>tablet pc</strong> from us, your information and all transaction data are ensured by using advanced security solution provider. The major credit cards are accepted. We try our best to make your shopping safe and easy. </div>
		  </div>
		  <p class="mclear"></p>
		  <div class="dl">
		    <div class="i1">Free Shipping and Fast Delivery</div>
		    <div class="i2">Once your payment is confirmed, your item(s) will be delivered within 1-2 working days. We use DHL, EMS and Regular Post Office to ship the products. All the <strong>Android Tablets</strong> and accessories are worldwide shipping. All our <strong>Android PADs</strong> can be shipped to all around the world. It just only takes 2 to 14 days from China to major destinations. </div>
		  </div>
		  <div class="dl">
		    <div class="i1">Quality Assurance</div>
		    <div class="i2">All of the products are brand new, top quality: absolutely no used products. All the products will be subject to strict quality test to make sure that the products sent to customers are qualified. We promised that any product in 30 days can be returned according to our return policy. </div>
		  </div>
		  <p class="mclear"></p>
		</div><!--list-->
    </div><!--site_tips-->
    <%}%>
</div><!--FootDiv-->