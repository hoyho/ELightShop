﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Func;

public partial class _manage_topics_manage : System.Web.UI.Page
{
    public string EventName = "topics";
    public string MainPName = "专题";
    public string SecPName = "专题";
    public int CurPage, PageCount;

    protected void Page_Init(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
        Models Md = new Models();
        DataTable Dtab = new DataTable();
        Dtab = Tc.ConnDate("select id,title,nviews,static,relea_time from " + TableName.db_topics + " order by relea_time desc", 0);

        PagedDataSource objPds = new PagedDataSource();
        objPds.DataSource = Dtab.DefaultView;
        objPds.AllowPaging = true;
        objPds.PageSize = 12;

        PageCount = objPds.PageCount;
        CurPage = Convert.ToInt32(Request.QueryString["Page"]);

        if (CurPage < 1)
        {
            CurPage = 1;
        }
        else if (CurPage > objPds.PageCount)
        {
            CurPage = objPds.PageCount;
        }

        objPds.CurrentPageIndex = CurPage - 1;

        Ft_List.DataSource = objPds;
        Ft_List.DataBind();
		Tc.CloseConn();
    }
}
