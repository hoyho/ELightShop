﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="arti_ctrl.aspx.cs" Inherits="_manage_arti_ctrl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=MainPName%>管理中心</title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="Include/Validator.js"></script>
<script type="text/javascript" src="Include/textLimitChk.js"></script>
</head>
<body>
<form id="xform" name="xform" runat="server" action="" method="post" onSubmit="return Validator.Validate(this,3);">
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram_Position">
  <tr>
    <td>您现在的位置：<a href="#"><%=MainPName%>管理中心</a> &gt;&gt; <a href="#"><%=SecPName%></a></td>
  </tr>
</table>
<table width="40%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram">
  <tr>
    <td align="center"><h2><%=MainPName%>管理中心----<%=SecPName%></h2></td>
  </tr>
</table>
  <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1">
    <tbody>
      <tr>
        <td colspan="2" class="BarStyleV1"><h3>管理 </h3></td>
      </tr>
      <tr>
        <td width="50%" valign="top" class="BarStyleV1">
		    <input type="hidden" name="editor_content" id="editor_content" value="" runat="server" />
            <iframe id="oEditor" src="include/IEL-Editor/editor.html?xHtml=editor_content" frameborder="0" scrolling="no" width="100%" height="360"></iframe>
		</td>
        <td width="50%" align="left" valign="top" class="BarStyleV1"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1" style="margin-top: 0px; border: none;">
            <tbody>
              <tr>
                <td width="100" align="right" valign="middle">名称：</td>
                <td align="left" valign="middle"><input id="titler" maxlength="120" size="35" name="titler" msg="" datatype="Require" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">所属分类：</td>
                <td align="left" valign="middle">
					<select name="tid" class="main" id="tid" datatype="Require"  msg="" runat="server">
					<option value="" selected="selected">--请选择分类--</option>
					</select>
				</td>
              </tr>
              <tr>
                <td align="right" valign="middle">文章设置：</td>
                <td align="left" valign="middle">置顶
                  <input name="ck_box_1" type="checkbox" id="ck_box_1" value="true" runat="server" />
                  推荐
                  <input name="ck_box_2" type="checkbox" id="ck_box_2" value="true" runat="server" />
                  最新
                  <input name="ck_box_3" type="checkbox" id="ck_box_3" value="true" runat="server" />
                  热点
                  <input name="ck_box_4" type="checkbox" id="ck_box_4" value="true" runat="server" />
				  审核
                  <input name="ck_box_5" type="checkbox" id="ck_box_5" value="true" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle" style="height: 32px">文章版权：</td>
                <td align="left" valign="middle" style="height: 32px">作者
                  <input id="auth_er" maxlength="60" size="15" name="auth_er" msg="" datatype="Require" runat="server" />
                  来源
                  <input name="sourc" id="sourc" size="15" maxlength="60" msg="" datatype="Require" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">发布者：</td>
                <td align="left" valign="middle">
                  <input name="editer" id="editer" size="15" maxlength="60" msg="" datatype="Require" runat="server" />
				  <input name="editer_cid" type="hidden" id="editer_cid" value="" runat="server" />
				</td>
              </tr>
              <tr>
                <td align="right" valign="middle">缩略图：</td>
                <td align="left" valign="middle"><iframe allowtransparency="true" id="PhotoUpload" name="PhotoUpload" runat="server" frameborder="0" width="290" scrolling="no" height="22"></iframe>
                  <br />
                  <span class="Tip_Mess">* 上传图片大小 475 X 164 dpi</span>
                  <input name="img" type="hidden" id="img" value="" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">摘要：</td>
                <td align="left" valign="middle"><textarea id="sim" name="sim" rows="6" cols="41" onKeyUp="Limitchk(this,70,250)" runat="server"></textarea></td>
              </tr>
              <tr>
                <td align="right" valign="middle">关键字：</td>
                <td align="left" valign="middle"><input id="keywords" maxlength="120" size="41" name="keywords" msg="" datatype="Require" runat="server" />
                  <br />
                  <span class="Tip_Mess">*如果有多个关键字，请用;分隔开关键字。</span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="center">
                    <input type="submit" value="提交" name="Submit" class="Btn_v1" id="Submit" />
                    <input id="action" type="hidden" value="ctrl_add" name="action" runat="server" />
                    <input id="actid" type="hidden" value="0" name="actid" runat="server" />
                    <input type="reset" value="重置" name="reset" class="Btn_v1" />
                </td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
</form>
</body>
</html>
