﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using YduCLB.SQL;

public partial class _manage_common_ustatic : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();

        Conn Tc = new Conn("level0");

        string nId = Request.QueryString["uid"];
        string sTable = Request.QueryString["lib"];
        string[] arrTable = new string[] { TableName.db_product, TableName.db_arti,TableName.db_topics };
        string sTitle = string.Empty;
        try
        {
            SqlDataReader drReader = Tc.ConnDate("select id,title from " + arrTable[Convert.ToInt16(sTable)] + " where id=" + nId);
            if (drReader.Read())
            {
                sTitle = drReader["title"].ToString() + "-" + drReader["id"].ToString();
                sTitle = Regex.Replace(sTitle, @"[^a-zA-Z0-9/s]", "-", RegexOptions.IgnoreCase);
                sTitle = Regex.Replace(sTitle, @"-+", "-", RegexOptions.IgnoreCase).ToLower();
                drReader.Close();
                SqlDataReader objUpData = Tc.ConnDate("update " + arrTable[Convert.ToInt16(sTable)] + " set static='" + sTitle + "' where id=" + nId);
                objUpData.Close();
                objUpData.Dispose();
                Response.Redirect("style/images/blank.gif", false);
            }
            else
            {
                Response.Redirect("style/images/pointv4.gif");
            }
            drReader.Dispose();
        }
        catch
        {
            Response.Redirect("style/images/pointv4.gif");
        }
    }
}
