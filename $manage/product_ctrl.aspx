﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="product_ctrl.aspx.cs" Inherits="_manage_product_ctrl" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><%=MainPName%>管理中心</title>
<link href="style/master.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="Include/Validator.js"></script>
<script type="text/javascript" src="Include/textLimitChk.js"></script>
<script type="text/javascript" src="jscript/ctrljs.js"></script>
</head>
<body>
<form id="xform" name="xform" runat="server" action="" method="post" onSubmit="return Validator.Validate(this,3);">
<table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram_Position">
  <tr>
    <td>您现在的位置：<a href="#"><%=MainPName%>管理中心</a> &gt;&gt; <a href="#"><%=SecPName%></a></td>
  </tr>
</table>
<table width="40%" border="0" align="center" cellpadding="0" cellspacing="0" id="MainFram">
  <tr>
    <td align="center"><h2><%=MainPName%>管理中心----<%=SecPName%></h2></td>
  </tr>
</table>
  <table width="96%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1">
    <tbody>
      <tr>
        <td colspan="2" class="BarStyleV1"><h3>管理 </h3></td>
      </tr>
      <tr>
        <td width="50%" valign="top" class="BarStyleV1">
            <div class="Tab-btn"><span id="tab-1" onClick="swtab(this);">产品概述</span><span id="tab-2" onClick="swtab(this);">√ 产品详细</span></div>
            <input type="hidden" name="editor_overview" id="editor_overview" value="" runat="server" />
            <input type="hidden" name="editor_details" id="editor_details" value="" runat="server" />
		    <div style=" height: 425px; overflow: hidden;">
		        <div id="tab-2-layer" style="display: block;"><iframe id="oEditor_" src="include/IEL-Editor/editor.html?xHtml=editor_details" frameborder="0" scrolling="no" width="100%" height="430"></iframe></div>
		        <div id="tab-1-layer" style="display: block;"><iframe id="oEditor" src="include/IEL-Editor/editor.html?xHtml=editor_overview" frameborder="0" scrolling="no" width="100%" height="430"></iframe></div>
		    </div>
		</td>
        <td width="50%" align="left" valign="top" class="BarStyleV1"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="TableSt1" style="margin-top: 0px; border: none;">
            <tbody>
              <tr>
                <td width="100" align="right" valign="middle">名称：</td>
                <td align="left" valign="middle"><input id="titler" maxlength="120" size="35" name="titler" msg="" datatype="Require" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">所属分类：</td>
                <td align="left" valign="middle">
					<select name="tid" class="main" id="tid" datatype="Require"  msg="" runat="server"></select>
					<input name="tid_disc" id="tid_disc" type="hidden" value="0"  runat="server" />
				</td>
              </tr>
              <tr>
                <td align="right" valign="middle">文章设置：</td>
                <td align="left" valign="middle">置顶
                  <input name="ck_box_1" type="checkbox" id="ck_box_1" value="true" runat="server" />
                  推荐
                  <input name="ck_box_2" type="checkbox" id="ck_box_2" value="true" runat="server" />
                  最新
                  <input name="ck_box_3" type="checkbox" id="ck_box_3" value="true" runat="server" />
                  热点
                  <input name="ck_box_4" type="checkbox" id="ck_box_4" value="true" runat="server" />
				  审核
                  <input name="ck_box_5" type="checkbox" id="ck_box_5" value="true" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">产品价格：</td>
                <td align="left" valign="middle">原价
                  <input name="was" id="was" size="15" maxlength="60" msg="" datatype="Require" runat="server" />
				  <img src="style/images/PointV4.gif" onClick="document.getElementById('was').value=parseInt((Math.random()*10+6))*10+parseInt(document.getElementById('price').value);" />
                  价格
                  <input name="price" id="price" size="15" maxlength="60" msg="" datatype="Require" runat="server" />
				</td>
              </tr>
              <tr>
                <td align="right" valign="middle">免运费：</td>
                <td align="left" valign="middle">
                  <input name="freeshipck" type="checkbox" id="freeshipck" value="true" runat="server" />
                  <span style="color:Green;">Free Shipping</span>
				</td>
              </tr>
              <tr>
                <td align="right" valign="middle">编辑：</td>
                <td align="left" valign="middle">
                  <input name="editer" id="editer" size="15" maxlength="60" msg="" datatype="Require" runat="server" />
				  <input name="editer_cid" type="hidden" id="editer_cid" value="" runat="server" />
				</td>
              </tr>
              <tr>
                <td align="right" valign="middle">缩略图：</td>
                <td align="left" valign="middle"><iframe allowtransparency="true" id="PhotoUpload" name="PhotoUpload" runat="server" frameborder="0" width="290" scrolling="no" height="22"></iframe>
                  <br />
                  <span class="Tip_Mess">* 上传图片大小 475 X 164 dpi</span>
                  <input name="img" type="hidden" id="img" value="" runat="server" /></td>
              </tr>
              <tr>
                <td align="right" valign="middle">预览图：</td>
                <td align="left" valign="middle"><textarea name="pview_img" id="pview_img" cols="0" rows="5" style="width: 99%;" wrap="virtual" runat="server"></textarea></td>
              </tr>
              <tr>
                <td align="right" valign="middle">简介：</td>
                <td align="left" valign="middle"><input type="hidden" name="siminfo" id="siminfo" value="" runat="server" /><iframe id="siminfo_frame" src="include/Sim-Editor/editor.html?xHtml=siminfo" frameborder="0" scrolling="no" width="100%" height="120"></iframe></td>
              </tr>
              <tr>
                <td align="right" valign="middle">关键字：</td>
                <td align="left" valign="middle"><input id="keywords" maxlength="120" size="48" name="keywords" msg="" datatype="Require" runat="server" />
                  <br />
                  <span class="Tip_Mess">*如果有多个关键字，请用;分隔开关键字。</span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td align="center">
                    <input type="submit" value="提交" name="Submit" class="Btn_v1" id="Submit" />
                    <input id="action" type="hidden" value="ctrl_add" name="action" runat="server" />
                    <input id="actid" type="hidden" value="0" name="actid" runat="server" />
                    <input type="reset" value="重置" name="reset" class="Btn_v1" />
                </td>
              </tr>
            </tbody>
          </table></td>
      </tr>
    </tbody>
  </table>
</form>
</body>
</html>
