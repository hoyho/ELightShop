﻿using System;
using System.IO;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

using YduCLB.Func;

public partial class _manage_upload_photoupload : System.Web.UI.Page
{
    //private const string SCRIPT_TEMPLATE = "<" + "script " + "type=\"text/javascript\">window.parent.photoUploadComplete('{0}', {1});" + "<" + "/script" + ">";
    private const string SCRIPT_TEMPLATE = "<span style=\"font-size:12px;\">{0}</span>";
    
    protected void Page_Init(object sender, EventArgs e)
    {
        WebChk.ChkAdmin();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            //Sleeping for 10 seconds, fake delay, You should not it try at home.
            //System.Threading.Thread.Sleep(10 * 1000);
            UploadPhoto();
        }
        else
        {
            hidepic.Value = Request.QueryString["pic"];
            hideurl.Value = "uploadfiles/" + Request.QueryString["intent"];
        }
    }

    private void UploadPhoto()
    {
        Other Ot = new Other();
        string script = string.Empty;
        string appdir = string.Empty;
        appdir = Ot.GetAppSetValue("AppDir");

        if ((filPhoto.PostedFile != null) && (filPhoto.PostedFile.ContentLength > 0))
        {
            if (!IsValidImageFile(filPhoto))
            {
                script = string.Format(SCRIPT_TEMPLATE, "文件类型不正确.");
            }
        }
        else
        {
            script = string.Format(SCRIPT_TEMPLATE, "请选择文件.");
        }

        if (string.IsNullOrEmpty(script))
        {
            //Uploaded file is valid, now we can do whatever we like to do, copying it file system,
            //saving it in db etc.

            //Your Logic goes here

            string StrFileName = filPhoto.PostedFile.FileName;
            //获取扩展名
            int i = StrFileName.LastIndexOf(".");
            string Extension = StrFileName.Substring(i);
            DateTime dt = DateTime.Now;
            string fname = dt.Year.ToString("x") + dt.Month.ToString("x") + dt.Day.ToString("x") + dt.Hour.ToString("x") + dt.Minute.ToString("x") + dt.Second.ToString("x") + dt.Millisecond.ToString("x");
            string fdir = hideurl.Value + dt.ToString("yyyy_MM") + "/";
            string picpath = "/" + fdir + fname + Extension;
            string fpath = Server.MapPath(appdir + fdir) + fname + Extension;
            if (!Directory.Exists(Server.MapPath(appdir + fdir)))
            {
                Directory.CreateDirectory(Server.MapPath(appdir + fdir));
            }

            filPhoto.PostedFile.SaveAs(fpath);

            script = string.Format(SCRIPT_TEMPLATE, "图片上传成功.");
            script = "<script>parent.document.forms[0]." + hidepic.Value + ".value='" + picpath + "'</script>" + script;
        }

        //Now inject the script which will fire when the page is refreshed.
        //ClientScript.RegisterStartupScript(this.GetType(), "uploadNotify", script);
        Response.Write("<style type=\"text/css\">*{padding: 6px; margin: 0px;}</style>" + script);
        Response.End();
    }

    private static bool IsValidImageFile(HtmlInputFile file)
    {
        try
        {
            using (Bitmap bmp = new Bitmap(file.PostedFile.InputStream))
            {
                return true;
            }
        }
        catch (ArgumentException)
        {
            //throws exception if not valid image
        }

        return false;
    }
}
