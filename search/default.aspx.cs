﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Front;
using YduCLB.Func;


public partial class search_default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
        GList xGList = new GList();
        FCtrl xFctrl = new FCtrl();

        ArrayList Result = new ArrayList();
        string[] aRate = xFctrl.Cvrate();
        string sData = string.Empty;
        string sCondi = string.Empty;
        double nPrice = 0;

        string sKeys = Request.Form["keys-input"] + Request.QueryString["keys"];

        if (sKeys == "")
        {
            Other.net_alert("Parameter error!", Si.siteurl);
        }
        else
        {
            sCondi = "title like '%" + sKeys + "%'";
        }

        Ft_CurrTi.Text = sKeys;
		Ft_PageTitle.Text = "Search for " + sKeys + ",  OfferTablets.com";

        DataTable DtabN = new DataTable();
        DataTable DtabM = new DataTable();

        int PageCount = 0;
        int CurPage = 0;
        int PageSize = 16;
        int nTop = 0;

        string TSQL_Table = TableName.db_product;
        string TSQL_Field = "id, static, title, was, price, small_pic";
        string TSQL_Condi = sCondi + " order by relea_time desc";

        try
        {
            DtabN = Tc.ConnDate("select id from " + TSQL_Table + " where " + TSQL_Condi, 0);

            PagedDataSource objPdsN = new PagedDataSource();
            objPdsN.DataSource = DtabN.DefaultView;
            objPdsN.AllowPaging = true;
            objPdsN.PageSize = PageSize;
            DtabN.Dispose();

            PageCount = objPdsN.PageCount;
            CurPage = Convert.ToInt32(Request.QueryString["Page"]);

            if (CurPage < 1)
            {
                CurPage = 1;
            }
            if (CurPage > PageCount)
            {
                CurPage = PageCount;
            }

            nTop = (CurPage - 1) * PageSize;
            DtabM = Tc.ConnDate("select top " + PageSize + " " + TSQL_Field + " from " + TSQL_Table + " where id not in (select top " + nTop.ToString() + " id from " + TSQL_Table + " where " + TSQL_Condi + ") and " + TSQL_Condi, 0);

            //sData = string.Empty;
            //Result = xGList.get_RecList(TableName.db_product, 1, 16, "1=1 order by relea_time desc", new string[] { "id", "static", "title", "was", "price", "small_pic" });
            //foreach (string[] aList in Result)
            //{
            //    nPrice = Convert.ToDouble(aList[3]) / Convert.ToDouble(aRate[0]);
            //    aList[3] = aRate[1] + Math.Round(nPrice, 2).ToString();

            //    nPrice = Convert.ToDouble(aList[4]) / Convert.ToDouble(aRate[0]);
            //    aList[4] = aRate[1] + Math.Round(nPrice, 2).ToString();

            //    sData = sData + String.Format("<ul><li class=\"i1\"><a href=\"/store/{1}.html\"><img src=\"{5}\" alt=\"\" width=\"160\" height=\"160\" /></a></li><li class=\"i2\"><a href=\"/store/{1}.html\" title=\"{2}\">{2}</a></li><li class=\"i3\">{3}</li><li class=\"i4\">{4}</li></ul>", aList) + "\n";
            //}

            for (int i = 0; i < DtabM.Rows.Count; i++)
            {
                nPrice = Convert.ToDouble(DtabM.Rows[i]["was"]) / Convert.ToDouble(aRate[0]);
                DtabM.Rows[i]["was"] = aRate[1] + Math.Round(nPrice, 2).ToString();

                nPrice = Convert.ToDouble(DtabM.Rows[i]["price"]) / Convert.ToDouble(aRate[0]);
                DtabM.Rows[i]["price"] = aRate[1] + Math.Round(nPrice, 2).ToString();
            }

            PagedDataSource objPdsM = new PagedDataSource();
            objPdsM.DataSource = DtabM.DefaultView;
            DtabM.Dispose();

            Ft_List.DataSource = objPdsM;
            Ft_List.DataBind();

            Ft_Page.Text = xFctrl.PageMe(CurPage, PageCount, "keys=" + sKeys);
        }
        catch
        {
            Response.Redirect("/");
        }

        sData = string.Empty;
        Result = xGList.get_RecList(TableName.db_product, 1, 5, "is_top=1 order by relea_time desc", new string[] { "id", "static", "title", "was", "price", "small_pic" });
        foreach (string[] aList in Result)
        {
            nPrice = Convert.ToDouble(aList[4]) / Convert.ToDouble(aRate[0]);
            aList[4] = aRate[1] + Math.Round(nPrice, 2).ToString();

            sData = sData + String.Format("<ul><li class=\"i1\"><a href=\"/store/{1}.html\"><img src=\"{5}\" alt=\"\" width=\"75\" height=\"75\" /></a></li><li class=\"i2\"><a href=\"/store/{1}.html\" title=\"{2}\">{2}</a><span>{4}</span></li><li class=\"mclear\"></li></ul>", aList) + "\n";
        }
        Ft_Tsale.Text = sData;
        
        Tc.CloseConn();
    }
}
