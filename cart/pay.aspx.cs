﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using YduCLB.SQL;

public partial class cart_pay : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();

        string uid = string.Empty;
        try { uid = Session["MemberID"].ToString(); }
        catch { uid = "0"; }

        string sShipMeth = Request.QueryString["shipping-method"];
        string sPayMeth = Request.QueryString["payment-method"];
		string sAct = Request.QueryString["act"];
		string sCtrl = string.Empty;
		string sReco = string.Empty;
        string sOrderNo = System.DateTime.Now.ToFileTime().ToString().Substring(9) + String.Format("{0:000}", System.DateTime.Now.Millisecond);

        if (Request.Cookies["MyCartIds"].Value != null)
        {
			SqlDataReader drReader = Tc.ConnDate("select id from " + TableName.db_cart + " where ident='" + Request.Cookies["MyCartIds"].Value + "' order by relea_time desc");
			
			if (drReader.Read())
            {
				if(sAct == "buynow")
				{
					sCtrl = "top 1 ";
					sReco = drReader["id"].ToString();
				}
                drReader.Close();
                drReader.Dispose();
                SqlDataReader objUpDataN = Tc.ConnDate("insert into " + TableName.db_order + "(orderno,proid,amount,nprice,remark) select "+sCtrl+"'" + sOrderNo + "',proid,amount,nprice,remark from " + TableName.db_cart + " where ident='" + Request.Cookies["MyCartIds"].Value + "'");
                objUpDataN.Close();
                objUpDataN.Dispose();
                SqlDataReader objUpDataM = Tc.ConnDate("insert into " + TableName.db_orderbasic + "(orderno,memid,subtotal,paymeth,shipmeth,status) values('" + sOrderNo + "', " + uid + ", '0', '" + sPayMeth + "', '" + sShipMeth + "', 'Unpaid')");
                objUpDataM.Close();
                objUpDataM.Dispose();
				if(sAct == "buynow")
				{
					SqlDataReader drReaderN = Tc.ConnDate("delete " + TableName.db_cart + " where ident='" + Request.Cookies["MyCartIds"].Value + "' and id=" + sReco);
					drReaderN.Close();
                	drReaderN.Dispose();
				}
				else
				{
					SqlDataReader drReaderN = Tc.ConnDate("delete " + TableName.db_cart + " where ident='" + Request.Cookies["MyCartIds"].Value + "'");
					drReaderN.Close();
                	drReaderN.Dispose();
				}
                
                
            }
        }
        Tc.CloseConn();
    }
}
