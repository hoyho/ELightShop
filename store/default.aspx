﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="store_default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
<script language="javascript" type="text/javascript">
var iKits={
	light_alert:function(tips){var obj01,obj02;if($3("#light_alert div.txt:0")){obj01=$3("#light_alert");obj02=$3("#light_fade")}else{var objN=document.createElement("div");var htmls="<div class=\"txt\">!</div>\n<div class=\"opera\"><span class=\"btn\" onclick=\"iKits.light_alert_close();\">CONFIRM</span></div>";objN.innerHTML=htmls;objN.setAttribute("id","light_alert");$3.box().appendChild(objN);$3("#light_alert div.txt:0").innerHTML=tips;var objM=document.createElement("div");objM.setAttribute("id","light_fade");$3.box().appendChild(objM);obj01=objN;obj02=objM;objN=objM=null}if(obj01!=undefined){obj01.style.display="block";obj01.style.left=parseInt(($3.box().clientWidth-obj01.clientWidth-18)/2)+"px";obj01.style.top=parseInt($3.elem().scrollTop+($3.elem().clientHeight/2)-(obj01.clientHeight/2)-18)+"px";obj02.style.display="block";obj02.style.width=$3.box().clientWidth+"px";obj02.style.height=$3.box().scrollHeight+"px";$3("#light_alert div.txt:0").innerHTML=tips;obj01=obj02=null}},
	light_alert_close:function(){$3("#light_alert").style.display="none";$3("#light_fade").style.display="none";$3("#light_fade").style.width=$3("#light_fade").style.height="5px"}
};

var iMath = {
    Mul : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) * Number(m); return (c / Math.pow(10, 4)) },
    Div : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) / Number(m); return (c.toFixed(2)) },
    Add : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) + Number(m); return (c / Math.pow(10, 2)) },
    Sub : function(n, m) { var c; n = Number(n).toFixed(2); m = Number(m).toFixed(2); n = n.replace(".", ""); m = m.replace(".", ""); c = Number(n) - Number(m); return (c / Math.pow(10, 2)) }
};

function up_price(str,n){
    rsign=$3("#hid_rsign").value;
    if(n==0){
        var nSign=str.indexOf("+");
        if(nSign != -1){
            str=str.substring(nSign+1);
            $3("#pros span.was em:0").innerHTML = rsign + iMath.Add( Number($3("#hid_was").value), Number(str) );
            $3("#pros span.price em:0").innerHTML = rsign + iMath.Add( Number($3("#hid_price").value), Number(str) );
            $3("#pros b.total em:0").innerHTML = rsign + iMath.Add( iMath.Mul(Number($3("#hid_price").value), parseInt($3("#quantity").value)), iMath.Add(Number($3("#hid_ship").value), Number(str)) );
        }
    }
    
    if(n==1){
        var nQty=parseInt(str);
        $3("#pros b.total em:0").innerHTML = rsign + iMath.Add( iMath.Mul(Number($3("#pros span.price em:0").innerHTML.replace(rsign,"")), nQty), Number($3("#hid_ship").value) ); 
    }
};

function add2cart(nid,n){
    var uparam="?uid="+nid;
    var snote,ubool=true;
    
    if($3("#xcolor")){
        if($3("#xcolor").value==""){
            snote="Color!"; ubool=false;
        }else{
            uparam=uparam+"&color="+$3("#xcolor").value;
        }
    }
    
    if($3("#xstorage")){
        if($3("#xstorage").value==""){
            snote="Storage!"; ubool=false;
        }else{
            uparam=uparam+"&store="+$3("#xstorage").value;
        }
    }
    
    if(ubool && nid!=0){
        uparam=uparam+"&qty="+$3("#quantity").value;
        uparam=uparam.replace("+","!");
		if(n==0){
			window.location.href="/cart/"+uparam;
		}
		if(n==1){
			window.location.href="/cart/"+uparam+"&act=buynow";
		}
    }
    
    if(!ubool && nid==0){
        $3("#xalert").style.display = $3("#xalert").style.display=="" ? "none":"";
        $3("#xalert_value").innerHTML=snote;
    }
};

function add2wish(nid){
    var recall_wish=function(param){
        var tips;
        switch(param){
            case "Error":
                tips="Uncertain Error!";
                break;
            case "NotSignIn":
                tips="You are not logged in. Please <a href=\"/signin/\">sign in</a>.";
                break;
            case "Exists":
                tips="You have been added to your wishlist.";
                break;
            case "Finish":
                tips="You have added it to your wishlist successfully.View <a href=\"/account/my-wish-lists.html\">wishlist</a>.";
                break;
            default:
                tips="Uncertain Error!";
                break;
        }
        iKits.light_alert(tips);
    };
    $3.ajax("/aider/update.aspx", "act=add2wish&stand=add&uid=" + nid + "&rnd=" + parseInt(Math.random() * 1000), "html", recall_wish)
}

function showtab(n){
	$3("#detail_content div.mpage").each(function(){
		$3(this).style.display="none";
	});
	$3("#detail_content ul.Ti_3|tabs a").each(function(){
		$3(this).className="nor";
	});
	
	$3("#detail_content div.mpage:"+n).style.display="block";
	$3.evtsrc().className="cur";
}

function swap_pics(){
    $3("#pics div.v1 div.i1 img:0").src=this.src;
}

function swshopnote(){
    $3.setcookie("status_shopnoteshow","noteshow_off");
    $3("#shopping-proc").style.display="none";
}

function sele_rati(way){
    var nrati;
    nrati=parseInt(this.className.replace("rati",""));
    if(way==0){
        $3("#ico_rati i:0").style.width=(nrati*22)+"px";
    }else{
        $3("#result_rati").value=nrati;
    }
}

$3.onload(function(){
    $3("#pics div.v1 div.i2 img").each(function(){
        if($3.isie){
			$3(this).attachEvent("onmouseover",function(){ swap_pics.call($3(this)) });
		}else{
			$3(this).addEventListener("mouseover",function(){ swap_pics.call($3(this)) });
		}
    });
    
    $3("#sele_rati i").each(function(){
        if($3.isie){
			$3(this).attachEvent("onmouseover",function(){ sele_rati.call($3(this),0) });
			$3(this).attachEvent("onmouseout",function(){ sele_rati.call($3(this),1) });
		}else{
			$3(this).addEventListener("mouseover",function(){ sele_rati.call($3(this),0) });
			$3(this).addEventListener("mouseout",function(){ sele_rati.call($3(this),1) });
		}
    });
    
    if($3.getcookie("status_shopnoteshow") == ""){
        $3("#shopping-proc").style.display="block";
    }
    
});
</script>
</head>
<body>
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="DetailDiv">
	<div class="xlocation"><span class="ico"></span><a href="/">Home</a> &gt; Store &gt; Android Tablet PCs &gt; <asp:Literal id="Ft_ProSort" runat="server"/> &gt; Item#: PAD16883<asp:Literal id="Ft_StockUnit" runat="server"/></div>
    <div class="SeleDiv">
    	<div class="pics" id="pics">
        	<div class="v1">
            	<div class="i1"><asp:Literal id="Ft_PviewPic" runat="server"/></div>
                <div class="i2"><asp:Literal id="Ft_PviewPics" runat="server"/></div>
            </div>
            <div class="v2"></div>
        </div>
        <div class="pros" id="pros">
			<h2><asp:Literal id="Ft_ProTi" runat="server"/></h2>
            <div class="v2">In stock.</div>
            <ul class="v3">
            	<li class="i1"><span class="was">Was: <em><asp:Literal id="Ft_ProWas" runat="server"/></em></span></li>
                <li class="i2"><span class="price"><em><asp:Literal id="Ft_ProPrice" runat="server"/></em></span><span class="save">Save: <asp:Literal id="Ft_ProSave" runat="server"/></span></li>
                <asp:Literal id="Ft_ProWeight" runat="server"/>
                <li class="i4">Shipping Cost: <b><asp:Literal id="Ft_ProShipCost" runat="server"/></b></li>
                <li class="i5">Total Price: <b class="total"><em><asp:Literal id="Ft_ProTotal" runat="server"/></em></b></li>
                <li class="i6"><asp:Literal id="Ft_AddToCart" runat="server"/></li>
                <li class="i7"><a href="#">Write a Review</a>&nbsp;&nbsp;|&nbsp;&nbsp;<span class="ico"></span><asp:Literal id="Ft_AddToWish" runat="server"/></li>
            </ul>
        </div>
        <div class="opti">
        	<h3>Options</h3>
            <ul>
            	<asp:Literal id="Ft_SeleOpti" runat="server"/>
                <li class="i2">
                    <span class="sele_lab">QTY:</span>
                    <select name="quantity" id="quantity" onchange="up_price(this.value,1);" runat="server"></select>
                </li>
                <li class="i3">Shipping Method:</li>
                <li class="i4"><span class="post_ioc"></span></li>
                <li class="i5"><asp:Literal id="Ft_BuyNow" runat="server"/><asp:Literal id="Ft_Hidden" runat="server"/></li>
            </ul>
            <div class="other">
            	<div class="i1"></div>
                <div class="i2">
                	<a href="#" class="s1"><b>share by email</b></a>
                    <a href="#" class="s2"><b>share at facebook</b></a>
                    <a href="#" class="s3"><b>share at twitter</b></a>
                </div>
                <p class="mclear"></p>
            </div>
        	<i class="angletl"></i>
            <i class="angletr"></i>
            <i class="anglebl"></i>
            <i class="anglebr"></i>
	  </div>
	  <p class="mclear"></p>
    </div><!--SeleDiv-->
	<div class="shopping-proc" id="shopping-proc" style="display: none;">
		<div class="tips"><img src="/stylecss/images/tips_05.gif" alt="Shopping Process" /></div>
		<div class="close">Buying on OfferTablets is Safe & Easy! <span class="ico" onclick="swshopnote();"></span></div>
	</div><!--shopping-proc-->
    <div class="RightDiv">
		<div class="detail_content" id="detail_content">
			<ul class="Ti_3 tabs">
				<li class="i1"><h3><asp:Literal id="Ft_Tabs" runat="server"/></h3></li>
				<li class="i2"></li>
				<li class="mclear"></li>
			</ul>
		    <asp:Literal id="Ft_ProOverview" runat="server"/>
		    <asp:Literal id="Ft_ProDescript" runat="server"/>
		    <div class="mpage" id="mpage03" style="display: none;">
                <p class="caption">OfferTablets does not endorse, recommend or sponsor any guest product reviews.</p>
                <div class="rating_statis_v1">
                	<ul>
                    	<li>5 star:<span class="ico"><i style="width: 50%;"></i></span><span class="num">0</span></li>
                        <li>4 star:<span class="ico"><i style="width: 30%;"></i></span><span class="num">2<i>(66%)</i></span></li>
                        <li>3 star:<span class="ico"><i style="width: 20%;"></i></span><span class="num">2<i>(66%)</i></span></li>
                        <li>2 star:<span class="ico"><i style="width: 10%;"></i></span><span class="num">2<i>(66%)</i></span></li>
                        <li>1 star:<span class="ico"><i style="width: 5%;"></i></span><span class="num">1<i>(66%)</i></span></li>
                    </ul>
                </div>
                <div class="rating_statis_v2">
                	<ul>
                    	<li class="i1">Average Rating:<span class="ico"><i style="width: 50%;"></i></span></li>
                        <li class="i2"><b>3</b> customer reviews <a href="#">Write your own review</a> </li>
                    </ul>
                </div>
                <p class="mclear line01"></p>
                <%--<div class="review_box">
                	<ul class="v1">
                    	<li class="i1">Fit very well, nice product</li>
                        <li class="i2">By <i>Finny</i>, 02/07/2012 <span class="ico" style="width: 80px;"></span></li>
                    </ul>
                    <p class="v2">helpful? <a href="#">yes</a><i class="yes">(1)</i> <a href="#">no</a><i class="no">(1)</i></p>
                    <p class="mclear line02"></p>
                    <div class="v3"></div>
                    <div class="v4">Fit very well, nice product, but I received this little bit bent, because packing was smaller than protector, so in the middle was bent. It can be see that in the middle is line, but its not so critical. Working very fine for this price is very good product!</div>
                    <p class="mclear"></p>
                    <div class="v5">
                    	<p class="i1">Reply by OfferTablets 02/07/2012</p>
                        <div class="i2">
                        	Good quality and cheap. A very worthy buy. I love the frosted screen protector as it doesn't shows my fingerprints clearly.
                        </div>
                        <i class="pointl"></i>
                        <i class="angletl"></i>
                        <i class="angletr"></i>
                        <i class="anglebl"></i>
                        <i class="anglebr"></i>
                    </div>
                </div>--%><!--review_box-->
                <div class="review_ctrl">
                	<h4>Write A Review</h4>
                    <p class="v1">Tell us what you think about this item and share your opinions with other people. Please make sure your review focuses only on this item. To maintain a respectful space for our customers to share their opinions, we will closely moderate all posted reviews for appropriateness in the first 24 hours.</p>
                	<ul class="v2">
                    	<li class="i1"><span class="tips">Your Rating:</span><span class="ico" id="ico_rati"><i style="width: 0px;"></i></span><span class="sele_rati" id="sele_rati"><i class="rati01"></i><i class="rati02"></i><i class="rati03"></i><i class="rati04"></i><i class="rati05"></i></span></li>
                        <li class="i2"><span class="tips">Review Title:</span><input name="" type="text" class="inputs" /></li>
                        <li class="i3"><span class="tips">Content:</span><textarea name="" cols="72" rows="6" class="inputs tarea"></textarea></li>
                        <li class="i4">
                            <span class="tips">&nbsp;</span><input name="" type="text" class="inputs" size="5" />
                            <img class="vcodes" onclick="src='/aider/codes.aspx?s='+Math.random()" src="/aider/codes.aspx" alt="verification code" />
                            <i>Type the numbers you see in the image on the right:</i>
                        </li>
                        <li class="i5"><span class="tips">&nbsp;</span><span class="btner"><input id="result_rati" name="result_rati" type="hidden" value="0" /><input type="submit" class="inputs_btn_st01" value="Submit" id="submit_btn" name="submit_btn" style="width: 80px;" /></span></li>
                    </ul>
                </div><!--review_ctrl-->
            </div><!--mpage03-->
	    </div>
    </div><!--RightDiv-->
    
    <div class="LeftDiv">
		<div class="box_rts">
			<ul class="Ti_2">
				<li class="i1"><h3>Related</h3></li>
				<li class="i2"></li>
				<li class="mclear"></li>
			</ul>
			<div class="dlist">
			    <asp:Literal id="Ft_ProRand" runat="server"/>
			</div>
		</div>
    </div><!--LeftDiv-->
    <p class="mclear"></p>

</div>
<!--DetailDiv-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</body>
</html>