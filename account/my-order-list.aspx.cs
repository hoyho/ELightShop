﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Func;

public partial class account_my_order_list : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int CurPage, PageCount;
        Conn Tc = new Conn();
        DataTable Dtab = new DataTable();

        string uid = string.Empty;
        try { uid = Session["MemberID"].ToString(); }
        catch { Response.Redirect("/signin/"); }
		
		Ft_PageTitle.Text = "My Account-My Order List, OfferTablets.com";
		
        try
        {
            Dtab = Tc.ConnDate("select id,orderno,subtotal,paymeth,shipmeth,status,relea_time,'_' stime from " + TableName.db_orderbasic + " where memid=" + uid + " order by relea_time desc", 0);

            for (int i = 0; i < Dtab.Rows.Count; i++)
            {
                Dtab.Rows[i]["stime"] = (Convert.ToDateTime(Dtab.Rows[i]["relea_time"])).ToString("mm/dd/yyyy", DateTimeFormatInfo.InvariantInfo);
            }

            PagedDataSource objPds = new PagedDataSource();
            objPds.DataSource = Dtab.DefaultView;
            objPds.AllowPaging = true;
            objPds.PageSize = 12;

            PageCount = objPds.PageCount;
            CurPage = Convert.ToInt32(Request.QueryString["page"]);

            if (CurPage < 1)
            {
                CurPage = 1;
            }
            else if (CurPage > objPds.PageCount)
            {
                CurPage = objPds.PageCount;
            }

            objPds.CurrentPageIndex = CurPage - 1;
            
            Ft_List.DataSource = objPds;
            Ft_List.DataBind();
            
        }
        catch
        {

        }
        Tc.CloseConn();
    }
}
