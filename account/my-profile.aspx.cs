﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Data;


public partial class account_my_profile : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {
        FCtrl xFctrl = new FCtrl();

        string sMonth = "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec";
        string[] arr_month = sMonth.Split(',');

        for (int i = 0; i < arr_month.Length; i++)
        {
            act0_month.Items.Add(new ListItem(arr_month[i], (i + 1).ToString()));
        }
        for (int i = 1; i < 32; i++)
        {
            act0_days.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        for (int i = 2000; i > 1931; i--)
        {
            act0_years.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        for (int i = 0; i < xFctrl.ArrCountry().Length / 2; i++)
        {
            act0_country.Items.Add(new ListItem(xFctrl.ArrCountry()[i], xFctrl.ArrCountry()[xFctrl.ArrCountry().Length / 2 + i]));
        }
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
        string uid=string.Empty;
        try { uid = Session["MemberID"].ToString(); }
        catch { Response.Redirect("/signin/"); }
		
		Ft_PageTitle.Text = "My Account-My Profile, OfferTablets.com";

        if (IsPostBack)
        {
            string sAct = entry.Value;
            string nickname_ = nickname.Value;
            int nResult = 0;
            
            if (sAct == "act_00")
            {
                DbCtrl Dc = new DbCtrl();

                string act0_firstname_ = act0_firstname.Value;
                string act0_lastname_ = act0_lastname.Value;
                string act0_birthday_ = act0_month.Value + "," + act0_days.Value + "," + act0_years.Value;
                string act0_country_ = act0_country.Value;
                string act0_remark_ = act0_remark.Value;

                string act0_gender_ = string.Empty;
                if (act0_gender01.Checked == true)
                {
                    act0_gender_ = "1";
                }
                if (act0_gender02.Checked == true)
                {
                    act0_gender_ = "2";
                }

                string act0_marital_ = string.Empty;
                if (act0_marital01.Checked == true)
                {
                    act0_marital_ = "1";
                }
                if (act0_marital02.Checked == true)
                {
                    act0_marital_ = "2";
                }
                if (act0_marital03.Checked == true)
                {
                    act0_marital_ = "3";
                }

                try
                {
                    string[] act0_fieldval = new string[]{"memid:"+uid,
                                                    "fname:"+act0_firstname_,
                                                    "lname:"+act0_lastname_,
                                                    "gender:"+act0_gender_,
                                                    "birthday:"+act0_birthday_,
                                                    "country:"+act0_country_,
                                                    "marital:"+act0_marital_,
                                                    "remark:"+act0_remark_};

                    SqlDataReader objDataReader = Tc.ConnDate("select id from " + TableName.db_membasic + " where memid=" + uid);
                    if (objDataReader.Read())
                    {
                        objDataReader.Dispose();
                        nResult = Dc.UpdateRecord(TableName.db_membasic, "memid=" + uid, act0_fieldval);
                    }
                    else
                    {
                        objDataReader.Dispose();
                        nResult = Dc.AddRecord(TableName.db_membasic, act0_fieldval);
                    }
                    if (nResult > 0)
                    {
                        Response.Redirect("/account/my-profile.html?status=E8981", false);
                    }
                    else
                    {
                        Response.Redirect("/account/my-profile.html?status=S9668", false);
                    }

                }
                catch
                {
                    Response.Redirect("/account/my-profile.html?status=E8982");
                }
            }/*profile_01*/

            if (sAct == "act_01")
            {
                string act1_reemail_ = act1_reemail.Value;
                SqlDataReader objUpDataM = Tc.ConnDate("update " + TableName.db_member + " set username=N'" + act1_reemail_ + "' where nickname=N'" + nickname_ + "' and id=" + uid);
                objUpDataM.Dispose();
                Response.Redirect("/account/my-profile.html?status=S9668&current=act1");
            }/*profile_01*/

            if (sAct == "act_02")
            {
                string act2_oldpassword_ = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(act2_oldpassword.Value, "MD5").ToLower().Substring(8, 16);
                SqlDataReader objDataReader = Tc.ConnDate("select * from " + TableName.db_member + " where nickname=N'" + nickname_ + "' and id=" + uid + " and passwd='" + act2_oldpassword_ + "'");
                if (objDataReader.Read())
                {
                    objDataReader.Dispose();
                    string act2_newpassword_ = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(act2_newpassword.Value, "MD5").ToLower().Substring(8, 16);
                    SqlDataReader objUpDataN = Tc.ConnDate("update " + TableName.db_member + " set passwd=N'" + act2_newpassword_ + "' where nickname=N'" + nickname_ + "' and id=" + uid);
                    objUpDataN.Dispose();
                    Response.Redirect("/account/my-profile.html?status=S9668&current=act2");
                }
                else
                {
                    Response.Redirect("/account/my-profile.html?status=E8983&current=act2");
                }
            }/*profile_01*/
        }
        else
        {
            SqlDataReader objDataReaderM = Tc.ConnDate("select * from " + TableName.db_member + " where id=" + uid);
            if (objDataReaderM.Read())
            {
                Ft_CurrEmail01.Text = Ft_CurrEmail02.Text = Ft_CurrEmail03.Text = objDataReaderM["username"].ToString();
                Ft_NickName.Text =nickname.Value = objDataReaderM["nickname"].ToString();
            }
            objDataReaderM.Dispose();
            SqlDataReader objDataReader = Tc.ConnDate("select * from " + TableName.db_membasic + " where memid=" + uid);
            if (objDataReader.Read())
            {
                act0_firstname.Value = objDataReader["fname"].ToString();
                act0_lastname.Value = objDataReader["lname"].ToString();
                string[] act0_arr_bday = objDataReader["birthday"].ToString().Split(',');
                act0_years.Value = act0_arr_bday[2];
                act0_month.Value = act0_arr_bday[0];
                int act0_nlen = 0;
                switch (act0_arr_bday[0])
                {
                    case "1":
                    case "3":
                    case "5":
                    case "7":
                    case "8":
                    case "10":
                    case "12":
                        act0_nlen = 31;
                        break;
                    case "2":
                        if (Convert.ToInt16(act0_arr_bday[2]) != 0 && Convert.ToInt16(act0_arr_bday[2]) % 4 == 0 && Convert.ToInt16(act0_arr_bday[2]) != 0)
                        {
                            act0_nlen = 29;
                        }
                        else
                        {
                            act0_nlen = 28;
                        }
                        break;
                    case "4":
                    case "6":
                    case "9":
                    case "11":
                        act0_nlen = 30;
                        break;
                    case "":
                        act0_nlen = 0;
                        break;
                }
                act0_days.Items.Clear();
                for (int i = 1; i < act0_nlen + 1; i++)
                {
                    act0_days.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                act0_days.Value = act0_arr_bday[1];
                act0_country.Value = objDataReader["country"].ToString();
                act0_remark.Value = objDataReader["remark"].ToString();
                switch (objDataReader["gender"].ToString())
                {
                    case "1":
                        act0_gender01.Checked = true;
                        break;
                    case "2":
                        act0_gender02.Checked = true;
                        break;
                    default:
                        break;
                }
                switch (objDataReader["marital"].ToString())
                {
                    case "1":
                        act0_marital01.Checked = true;
                        break;
                    case "2":
                        act0_marital02.Checked = true;
                        break;
                    case "3":
                        act0_marital03.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            objDataReader.Dispose();
            string sLalert = "<div><script language=\"javascript\" type=\"text/javascript\">iKits.light_alert(\"{$tips}\");</script></div>";
            string errors = Request.QueryString["status"];
            switch (errors)
            {
                case "E8981":
                    Ft_Lalert.Text = sLalert.Replace("{$tips}", "Operation Error!");
                    break;
                case "E8982":
                    Ft_Lalert.Text = sLalert.Replace("{$tips}", "Operation Error!");
                    break;
                case "E8983":
                    Ft_Lalert.Text = sLalert.Replace("{$tips}", "The old password entered was incorrect.");
                    break;
                case "S9668":
                    Ft_Lalert.Text = sLalert.Replace("{$tips}", "Update is successful!");
                    break;
                default:
                    break;
            }
        }
        Tc.CloseConn();
    }
}
