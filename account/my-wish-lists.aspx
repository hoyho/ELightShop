﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="my-wish-lists.aspx.cs" Inherits="account_my_wish_lists" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
<script language="javascript" type="text/javascript">
var del8rec=function(){
    var elem_del8rec=$3(this);
    if (elem_del8rec.innerHTML == "Remove") {
        $3("#hidctrl").src = "udate.aspx?act=wishlist-del!rec&stand=del&uid=" + elem_del8rec.name.replace("pro-","") + "&rnd=" + parseInt(Math.random() * 1000);
        elem_del8rec.innerHTML = "Recovery";
    } else if (elem_del8rec.innerHTML == "Recovery") {
        $3("#hidctrl").src = "udate.aspx?act=wishlist-del!rec&stand=rec&uid=" + elem_del8rec.name.replace("pro-","") + "&rnd=" + parseInt(Math.random() * 1000);
        elem_del8rec.innerHTML = "Remove";
    }
    elem_del8rec=null;
};

$3.onload(function(){
    $3("#wish_lists a.del8rec_link").each(function(){
        $3(this).click(del8rec);
    });
});
</script>
</head>
<body>
<form id="xform" runat="server" action="">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="UserCen">
	<div class="xlocation"><span class="ico"></span><a href="#">Home</a> &gt; <a href="#">Account</a></div>
	<Stencil:AccLeftTemp id="AccLeftTemp_" runat="server" />
    <div class="RightDiv">
        <div class="acc_box">
            <div class="wish_lists" id="wish_lists">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="table_st02">
                <thead>
                    <tr>
                        <th width="15%" align="center" style="height: 16px">SKU</th>
                        <th align="center" style="height: 16px">Item</th>
                        <th width="12%" align="center" style="height: 16px">Unit Price</th>
                        <%--<th width="10%" align="center" style="height: 16px">Quantity</th>
                        <th width="12%" align="center" style="height: 16px">Total Price</th>--%>
                        <th width="10%" align="center" style="height: 16px">Operation</th>
                    </tr>
                </thead>
                <tbody>
                    <%--<tr>
                        <td align="center"><a href="#">CE063B</a></td>
                        <td>
                            <div class="pro_pic"><a href="#"><img width="45" height="45" alt="" src="/uploadfiles/co_pros/2011_12/7dbc817212b2e.jpg" /></a></div>
                            <div class="pro_txt">
                                <h2 class="ti"><a href="#">Furious - 1.2GHz Android 2.3 Tablet with 8 Inch Touch Screen (1080P)</a></h2>
                                <p class="remark">store:8G color:white </p>
                            </div>
                        </td>
                        <td align="center"><span class="price">US $394.94</span></td>
                        <td align="center">6</td>
                        <td align="center" class="eyec"><span class="result">US $394.94</span></td>
                        <td align="center"><a href="javascript:void(0);" class="del8rec_link" name="pro-152">Remove</a></td>
                    </tr>--%>
                    <asp:Repeater id="Ft_List" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td align="center">CE063B</td>
                            <td>
                                <div class="pro_pic"><a href="/store/<%#Eval("static")%>.html"><img width="45" height="45" alt="" src="<%#Eval("small_pic")%>" /></a></div>
                                <div class="pro_txt">
                                    <h2 class="ti"><a href="/store/<%#Eval("static")%>.html"><%#Eval("title")%></a></h2>
                                </div>
                            </td>
                            <td align="center"><span class="price"><%#Eval("xprice")%></span></td>
                            <td align="center"><a href="javascript:void(0);" class="del8rec_link" name="pro-<%#Eval("id")%>">Remove</a></td>
                        </tr>
                    </ItemTemplate>
                    </asp:Repeater>
                </tbody>
                </table>
                <div><img id="hidctrl" class="hidctrl" alt="blank ctrl" src="/stylecss/images/blank.gif" /></div>
            </div>
        </div>
    </div><!--RightDiv-->
    <p class="mclear"></p>
    
</div><!--UserCen-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
<asp:Literal id="Ft_Lalert" runat="server"/>
</form>
</body>
</html>