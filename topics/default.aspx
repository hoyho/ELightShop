﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="topics_default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<form id="xform" runat="server">
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="PromTopic">
    <div class="xlocation"><span class="ico"></span><a href="/">Home</a> &gt; Android Tablet PC &gt; Hot Topics &gt; Details</div>
    <div class="PromEmbo">
        <asp:Literal id="Ft_Topic" runat="server"/>
    </div><!--PromEmbo-->
</div>
<!--PromTopic-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</form>
</body>
</html>
