﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using YduCLB.SQL;
using YduCLB.Func;

public partial class help_default : System.Web.UI.Page
{
    public bool read = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        int nId = Convert.ToInt16(Request.QueryString["uid"]);
        string sHelpCateList = string.Empty;
        string sHelpList = string.Empty;
        

        Conn Tc = new Conn();
        
        DataTable Dtab = new DataTable();
        Dtab = Tc.ConnDate("select id,cate_name from " + TableName.db_articate + " where nparent=3 order by nord desc", 0);

        for (int i = 0; i < Dtab.Rows.Count; i++)
        {
            //sHelpCateList = sHelpCateList + "<h4><a href=\"/help/category-" + Dtab.Rows[i]["id"] + ".html\">" + Dtab.Rows[i]["cate_name"] + "</a></h4>\n";
            sHelpCateList = sHelpCateList + "<h4>" + Dtab.Rows[i]["cate_name"] + "</h4>\n";
            SqlDataReader drReaderN = Tc.ConnDate("select title,static from " + TableName.db_arti + " where cateid=" + Dtab.Rows[i]["id"]);
            while (drReaderN.Read())
            {
                sHelpList = sHelpList + "<li><a href=\"/help/" + drReaderN["static"] + ".html\">" + drReaderN["title"] + "</a></li>\n";
            }
            if (drReaderN.HasRows)
            {
                sHelpCateList = sHelpCateList + "<ul>" + sHelpList + "</ul>";
            }
            sHelpList = string.Empty;
            drReaderN.Close();
            drReaderN.Dispose();
        }


        Dtab.Dispose();

        Ft_HelpList.Text = sHelpCateList;
		Ft_PageTitle.Text = "Help Center, OfferTablets.com";

        if (nId > 0)
        {
            try
            {
                SqlDataReader drReaderM = Tc.ConnDate("select id,title,content from " + TableName.db_arti + " where id=" + nId.ToString());
                if (drReaderM.Read())
                {
					Ft_PageTitle.Text = drReaderM["title"].ToString()+"-Help Center, OfferTablets.com";
                    Ft_HelpHtml.Text = drReaderM["content"].ToString();
                    read = true;
                }
                drReaderM.Close();
                drReaderM.Dispose();
                
            }
            catch
            {
                Other.net_alert("Parameter error!", Si.siteurl + "help/");
            }
        }
        else
        {

        }

        Tc.CloseConn();
    }
}
