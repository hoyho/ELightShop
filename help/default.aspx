﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="help_default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title><asp:Literal id="Ft_PageTitle" runat="server"/></title>
<Stencil:BasisTemp id="BasisTemp_" runat="server" />
</head>
<body>
<Stencil:HeadTemp id="HeadTemp_" runat="server" />
<div id="HelpDiv">
  <div class="xlocation"><span class="ico"></span><a href="#">Home</a> &gt; <a href="#">Help</a></div>
  <div class="RightDiv">
    <%if(read == true){%>
  	<div class="help_content"><asp:Literal id="Ft_HelpHtml" runat="server"/></div>
  	<%}else{%>
    <div class="help_guide">
      <div class="issuse">
          <h2>Top issues</h2>
          <ul>
            <li><a href="#">Does Focalprice.com offer free shipping service to any product?</a></li>
            <li><a href="#">Can I change my order once it has been processed?</a></li>
            <li><a href="#">How can I check out my order status?</a></li>
            <li><a href="#">Can I get the tracking number freely for any order?</a></li>
            <li><a href="#">What happen when My order status is back order?</a></li>
            <li><a href="#">What is Focalprice's return process?</a></li>
            <li><a href="#">How can I get more Focal points to exchange coupon?</a></li>
          </ul>
      </div>
      <div class="mlist">
          <dl class="v1">
            <dt>Shopping Guide</dt>
            <dd>Useful shipping &amp; delivery information </dd>
          </dl>
          <dl class="v2">
            <dt>Ordering</dt>
            <dd>Guidance to the safer, easier payment</dd>
          </dl>
          <dl class="v3">
            <dt>Shipping &amp; Delivery</dt>
            <dd>Simple, clear, convenient ordering process</dd>
          </dl>
          <dl class="v4">
            <dt>Returns &amp; Refunds</dt>
            <dd>Getting extra discount and enjoying shopping</dd>
          </dl>
          <dl class="v5">
            <dt>Security &amp; Privacy</dt>
            <dd>Secure online shopping with standard product's warrenty</dd>
          </dl>
          <dl class="v6">
            <dt>Company Info</dt>
            <dd>Fast solutions for replacement and refund</dd>
          </dl>
          <p class="mclear"></p>
      </div>
    </div>
    <%}%>
  </div>
  <!--RightDiv-->
  <div class="LeftDiv">
    <div class="help_cate">
      <asp:Literal id="Ft_HelpList" runat="server"/>
    </div>
  </div>
  <!--LeftDiv-->
  <p class="mclear"></p>
</div>
<!--DetailDiv-->
<Stencil:FootTemp id="FootTemp_" runat="server" />
</body>
</html>