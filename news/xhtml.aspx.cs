﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;

using YduCLB.Func;
using YduCLB.Front;
using YduCLB.SQL;



public partial class news_xhtml : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Conn Tc = new Conn();
        GList xGList = new GList();
		FCtrl xFctrl = new FCtrl();

        ArrayList Result = new ArrayList();
        string[] aRate = xFctrl.Cvrate();
        string sData = string.Empty;
		string sPageTi = string.Empty;
        double nPrice = 0;
		
        int nId = Convert.ToInt16(Request.QueryString["uid"]);
        if (nId > 0)
        {
            try
            {
                SqlDataReader drReader = Tc.ConnDate("select id,title,content,relea_time,source,editor from " + TableName.db_arti + " where id=" + nId.ToString());
                if (drReader.Read())
                {
				  	sPageTi = drReader["title"].ToString();
                    Ft_ArtTi.Text = drReader["title"].ToString();
                    Ft_ArtSource.Text = drReader["source"].ToString();
                    //Ft_ArtEditor.Text = drReader["editor"].ToString();
                    Ft_ArtTime.Text = drReader["relea_time"].ToString();
                    Ft_ArtContent.Text = drReader["content"].ToString();
                }
                drReader.Close();
                drReader.Dispose();
                FtRelart.Text = xFctrl.Relart(nId.ToString(), "cateid=5", TableName.db_arti);
            }
            catch
            {
                Other.net_alert("Parameter error!", Si.siteurl + "news/");
            }
        }
		
		sData = string.Empty;
        Result = xGList.get_RecList(TableName.db_arti, 1, 6, "is_rec=1 order by relea_time desc", new string[] { "id", "static", "title" });
        foreach (string[] aList in Result)
        {
            sData = sData + String.Format("<li><a href=\"{1}.html\">{2}</a></li>", aList) + "\n";
        }
        Ft_RecomNews.Text = sData;

        sData = string.Empty;
        Result = xGList.get_RecList(TableName.db_product, 1, 3, "is_top=1 order by relea_time desc", new string[] { "id", "static", "title", "was", "price", "small_pic" });
        foreach (string[] aList in Result)
        {
            nPrice = Convert.ToDouble(aList[4]) / Convert.ToDouble(aRate[0]);
            aList[4] = aRate[1] + Math.Round(nPrice, 2).ToString();

            sData = sData + String.Format("<ul><li class=\"i1\"><a href=\"/store/{1}.html\"><img src=\"{5}\" alt=\"\" width=\"75\" height=\"75\" /></a></li><li class=\"i2\"><a href=\"/store/{1}.html\" title=\"{2}\">{2}</a><span>{4}</span></li><li class=\"mclear\"></li></ul>", aList) + "\n";
        }
        Ft_Tsale.Text = sData;
		Ft_PageTitle.Text = sPageTi + " | OfferTablets.com";
        
        Tc.CloseConn();
    }
}
